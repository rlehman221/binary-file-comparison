/*
* Filename : File_Comparator.c
* ----------------------------
* Description:
*       Compare two binary files using command line parameters and 
*       find the offset at which the first difference occurs along
*		with the first 16 bytes from both files starting at that offset.
*		Also keep track of the CPU execution time for the comparison.
* 
* Author           : Ryan Lehman       
* Created          : 09-14-2018
*
* Last Modified By : Ryan Lehman
* Last Modified On : 09-17-2018
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define BUFFER_SIZE (4000)
void compareFiles(FILE *fp1, FILE *fp2, char *argv[]);


int main(int argc, char *argv[])
{
	FILE *fp1,*fp2;

	// Only two arguments should be entered
	if (argc < 3){
		printf("Not enough arguments entered \n");
		return 1;
	} else if (argc > 3) {
		printf("To many arguments entered \n");
		return 1;
	} else {
		// Check to make sure both files are valid to read from
		fp1 = fopen(argv[1], "r");
		if (fp1 == NULL) {
			printf("Error when trying to open file: %s\n", argv[1]);
			return 1;
		}

		fp2 = fopen(argv[2], "r");
		if (fp2 == NULL) {
			printf("Error when trying to open file: %s\n", argv[2]);
			return 1;
		}

		clock_t clock_cycles;
		clock_cycles = clock();
		compareFiles(fp1,fp2,argv);
		// Converts clock cycles to seconds using system-specific clock ticks per second
		double execution_time = ((double)clock_cycles/CLOCKS_PER_SEC);
		printf("==========================================\n");
		printf("Comparison executed in: %f seconds \n", execution_time);
		printf("==========================================\n");
	}
	return 0;
}

/*
 * Function: compareFiles
 * ----------------------------
 *   Description:
 *			Compares two files by storing the content (bytes) in separate buffers 
 *			and then looping through both structures to see if a difference occurs. 
 *			Also prints an offset byte at the difference between both files along 
 *			with the first 16 bytes of both files after the offset.
 *
 *   fp1: file 1 pointer
 *   fp2: file 2 pointer
 *
 *   Returns: Void
 */
void compareFiles(FILE *fp1, FILE *fp2, char *argv[])
{
	char ch1,ch2;
	int flag,byteCounter,i= 0;
	long index_counter = 0;

	// Stores the bytes of the file into a buffer
	fseek(fp1, 0L, SEEK_END);
	long file1_Size = ftell(fp1);
	char *file1_buffer = malloc(BUFFER_SIZE*sizeof(char));
	rewind(fp1);

	fseek(fp2, 0L, SEEK_END);
	long file2_Size = ftell(fp2);
	char *file2_buffer = malloc(BUFFER_SIZE*sizeof(char));
	rewind(fp2);

	// Copies a certain number of bytes from each files into the designated buffers
	while ((index_counter < file1_Size) && (index_counter < file2_Size) && (byteCounter < 16)) {
		fread(file1_buffer, sizeof(char), BUFFER_SIZE, fp1);
		fread(file2_buffer, sizeof(char), BUFFER_SIZE, fp2);
		// Compares both buffers to see if they are equal
		if (memcmp(file1_buffer, file2_buffer, BUFFER_SIZE) != 0) {
			
			// Loops through both buffers to see when the difference occurs
			while ((i < BUFFER_SIZE) && (index_counter < file1_Size) && (index_counter < file2_Size) && (byteCounter < 16)) {
				if (flag) {
					byteCounter++;
					printf ("%s%2d%10c%x%22c%x\n", "Byte:", byteCounter, 'x',file1_buffer[i], 'x', file2_buffer[i]);
				} else if (file1_buffer[i] != file2_buffer[i]) {
					flag = 1;
					printf("==========================================\n");
					printf("Offset at byte: %ld\n", index_counter);
					printf("==========================================\n");
					byteCounter++;
					printf ("%14s%s%10s%s\n", argv[1], " value (hex)", argv[2], " value (hex)");
					printf ("%s%2d%10c%x%22c%x\n", "Byte:", byteCounter, 'x',file1_buffer[i], 'x', file2_buffer[i]);
				}
				index_counter++;
				i++;
			}
			i = 0;
		} else {
			index_counter += BUFFER_SIZE;
		}
	}

	if (flag == 0 && file1_Size != file2_Size) {
		printf("Both files contain the same elements but one file is larger than the other\n");
	} else if (flag == 0) {
		printf("Both Files Are The Same\n");
	}

	free(file1_buffer);
	free(file2_buffer);
}